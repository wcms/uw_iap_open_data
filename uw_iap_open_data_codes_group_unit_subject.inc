<?php

/**
 * @file
 * Functions for Group, Unit, and Subject Codes.
 */

/**
 * Page callback.
 */
function uw_iap_open_data_codes_group_unit_subject_page() {
  menu_tree_set_path('main-menu', drupal_get_normal_path('university-data-and-statistics/glossaries'));

  $page = array();

  $rows = array();
  $groups = array();
  $data = uw_value_lists_uw_api_query('v2/codes/groups.json');
  foreach ($data->data as $item) {
    $groups[$item->group_code] = $item->group_full_name;
    $row = array($item->group_code, $item->group_short_name, $item->group_full_name);
    $rows[] = array('data' => $row, 'no_striping' => TRUE);
  }
  $page['groups']['head'] = array(
    '#markup' => '<h2>Groups (e.g. faculties)</h2><p>Count: ' . count($rows) . '.</p>',
  );
  $page['groups']['table'] = array(
    '#theme' => 'table',
    '#header' => array('Group Code', 'Group Short Name', 'Group Full Name'),
    '#rows' => $rows,
  );

  $rows = array();
  $units = array();
  $data = uw_value_lists_uw_api_query('v2/codes/units.json');
  foreach ($data->data as $item) {
    $units[$item->unit_code] = $item->unit_full_name;
    $row = array($item->unit_code, $item->unit_short_name, $item->unit_full_name, '<abbr title="' . $groups[$item->group_code] . '">' . $item->group_code . '</abbr>');
    $rows[] = array('data' => $row, 'no_striping' => TRUE);
  }
  $page['units']['head'] = array(
    '#markup' => '<h2>Units (e.g. departments)</h2><p>Each unit is part of a group. Count: ' . count($rows) . '.</p>',
  );
  $page['units']['table'] = array(
    '#theme' => 'table',
    '#header' => array('Unit Code', 'Unit Short Name', 'Unit Full Name', 'Group Code'),
    '#rows' => $rows,
  );

  $rows = array();
  $data = uw_value_lists_uw_api_query('v2/codes/subjects.json');
  foreach ($data->data as $item) {
    $row = array($item->subject, $item->description, '<abbr title="' . $units[$item->unit] . '">' . $item->unit . '</abbr>', '<abbr title="' . $groups[$item->group] . '">' . $item->group . '</abbr>');
    $rows[] = array('data' => $row, 'no_striping' => TRUE);
  }
  $page['subject']['head'] = array(
    '#markup' => '<h2>Subjects</h2><p>Each subject is owned by a unit which is part of a group. Count: ' . count($rows) . '.</p>',
  );
  $page['subject']['table'] = array(
    '#theme' => 'table',
    '#header' => array('Subject Code', 'Subject Name', 'Unit Code', 'Group Code'),
    '#rows' => $rows,
  );

  return $page;
}
