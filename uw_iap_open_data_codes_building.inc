<?php

/**
 * @file
 * Functions for Building Codes.
 */

/**
 * Page callback.
 */
function uw_iap_open_data_codes_building_page() {
  menu_tree_set_path('main-menu', drupal_get_normal_path('university-data-and-statistics/glossaries/space-glossaries'));

  $page = array();

  $rows = array();
  $data = uw_value_lists_uw_api_query('v2/buildings/list.json');
  foreach ($data->data as $item) {
    $coord = FALSE;
    if (is_numeric($item->latitude) && is_numeric($item->longitude)) {
      $coord = (float) $item->latitude . ',' . (float) $item->longitude;
    }
    $row = array(
      $item->building_id,
      $item->building_code,
      $item->building_name,
      $coord ? l($coord, 'https://maps.google.ca/maps', array('query' => array('q' => $coord))) : '',
    );
    $rows[] = array('data' => $row, 'no_striping' => TRUE);
  }
  $page['count'] = array('#markup' => '<p>Count: ' . count($rows) . '</p>');
  $page['table'] = array(
    '#theme' => 'table',
    '#header' => array('Building Id (unique for each)', 'Building Code (may be shared by related buildings)', 'Building Name', 'Coordinates (with map link)'),
    '#rows' => $rows,
  );

  return $page;
}
